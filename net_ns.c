/*
 * Kostak A Simple Container Example
 *
 * Copyright (C) 2020 Didiet Noor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "mount_ns.h"

#define PAYLOAD_SIZE 1024
#define NLMSG_TAIL(n) \
  ((struct rtattr *)(((uint8_t *)(n)) + NLMSG_ALIGN((n)->nlmsg_len)))

struct nl_req
{
  struct nlmsghdr n;
  struct ifinfomsg i;

  uint8_t payload[PAYLOAD_SIZE];
};

struct nla_req
{
  struct nlmsghdr n;
  struct ifaddrmsg ifa;

  uint8_t payload[PAYLOAD_SIZE];
};

struct nlr_req
{
  struct nlmsghdr n;
  struct rtmsg r;

  uint8_t payload[PAYLOAD_SIZE];
};

struct nlf_req
{
  struct nlmsghdr n;
  struct nfgenmsg f;

  uint8_t payload[PAYLOAD_SIZE];
};

static int addattr_l(struct nlmsghdr *n,
                     int maxlen, __u16 type,
                     const void *data, __u16 alen)
{
  const __u16 len = RTA_LENGTH(alen);
  const __u32 newlen = NLMSG_ALIGN(n->nlmsg_len) + RTA_ALIGN(len);

  if (newlen > maxlen)
  {
    errno = ENOSPC;
    return -1;
  }

  struct rtattr *attr = NLMSG_TAIL(n);
  attr->rta_len = len;
  attr->rta_type = type;

  void *rta_data = RTA_DATA(attr);

  memcpy(rta_data, data, alen);

  n->nlmsg_len = newlen;

  return 0;
}

static inline int addattr_uint32(struct nlmsghdr *n, int maxlen,
                                 __u16 type, const uint32_t value)
{
  return addattr_l(n, maxlen, type, &value, sizeof(uint32_t));
}

static inline int addattr_string(struct nlmsghdr *n, int maxlen,
                                 __u16 type, const char *str)
{
  return addattr_l(n, maxlen, type, str, strlen(str) + 1);
}

static struct rtattr *addattr_nest(struct nlmsghdr *n, int maxlen, __u16 type)
{
  struct rtattr *attr = NLMSG_TAIL(n);

  if (-1 == addattr_l(n, maxlen, type, NULL, 0))
  {
    DIE("Failed to add attributes\n");
  }
  return attr;
}

static void addattr_nest_end(struct nlmsghdr *n, struct rtattr *nest)
{
  const void *tail = NLMSG_TAIL(n);
  nest->rta_len = (uint8_t *)tail - (uint8_t *)nest;
}

static ssize_t read_response(int fd, struct msghdr *msg, char **response)
{
  struct iovec *iov = msg->msg_iov;
  iov->iov_base = *response;
  iov->iov_len = PAYLOAD_SIZE;

  const ssize_t resp_len = recvmsg(fd, msg, 0);

  if (0 == resp_len)
  {
    DIE("EOF of NETLINK\n");
  }

  if (0 > resp_len)
  {
    DIE("Netlink receive error: %m\n");
  }

  return resp_len;
}

static void check_response(int sock_fd)
{
  struct iovec iov;

  struct msghdr msg = {
      .msg_name = NULL,
      .msg_namelen = 0,
      .msg_iov = &iov,
      .msg_iovlen = 1,
  };

  char *resp = alloca(PAYLOAD_SIZE);

  ssize_t resp_len = read_response(sock_fd, &msg, &resp);

  struct nlmsghdr *hdr = (struct nlmsghdr *)resp;

  int nlmsglen = hdr->nlmsg_len;
  int datalen = nlmsglen - (sizeof(*hdr));

  if (datalen < 0 || nlmsglen > resp_len)
  {
    if (msg.msg_flags & MSG_TRUNC)
    {
      DIE("Received truncated message");
    }

    DIE("MALFORMED MESSAGE");
  }

  if (hdr->nlmsg_type == NLMSG_ERROR)
  {
    struct nlmsgerr *err = (struct nlmsgerr *)NLMSG_DATA(hdr);

    if (datalen < sizeof(struct nlmsgerr))
    {
      LOG("Error truncated");
    }

    if (err->error)
    {
      errno = -err->error;
      DIE("RTNETLink Error: %m");
    }
  }
}

static void send_nlmsg_with_ack(int sock_fd, struct nlmsghdr *n, bool ack)
{
  struct iovec iov = {
      .iov_base = n,
      .iov_len = n->nlmsg_len};

  struct msghdr msg = {
      .msg_name = NULL,
      .msg_namelen = 0,
      .msg_iov = &iov,
      .msg_iovlen = 1};

  n->nlmsg_seq++;

  ssize_t status = sendmsg(sock_fd, &msg, 0);
  if (status < 0)
    DIE("cannot talk to rtnetlink: %m\n");

  if (ack)
  {
    check_response(sock_fd);
  }
}

static inline void send_nlmsg(int sock_fd, struct nlmsghdr *n)
{
  send_nlmsg_with_ack(sock_fd, n, true);
}

static int create_socket(int domain, int type, int protocol)
{
  int sock_fd = socket(domain, type, protocol);

  if (sock_fd < 0)
  {
    DIE("Cannot open socket: %m");
  }

  return sock_fd;
}

static void create_veth(int sock_fd, const char *ifname, const char *peername)
{
  __u16 flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_EXCL | NLM_F_ACK;

  struct nl_req req = {
      .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg)),
      .n.nlmsg_flags = flags,
      .n.nlmsg_type = RTM_NEWLINK,
      .i.ifi_family = PF_NETLINK,
  };

  struct nlmsghdr *n = &req.n;
  int maxlen = sizeof(req);

  addattr_l(n, maxlen, IFLA_IFNAME, ifname, strlen(ifname) + 1);

  struct rtattr *linfo =
      addattr_nest(n, maxlen, IFLA_LINKINFO);
  addattr_l(&req.n, sizeof(req), IFLA_INFO_KIND, "veth", 5);

  struct rtattr *linfodata =
      addattr_nest(n, maxlen, IFLA_INFO_DATA);
  struct rtattr *peerdata =
      addattr_nest(n, maxlen, VETH_INFO_PEER);

  n->nlmsg_len += sizeof(struct ifinfomsg);

  addattr_l(n, maxlen, IFLA_IFNAME, peername, strlen(peername) + 1);

  addattr_nest_end(n, peerdata);
  addattr_nest_end(n, linfodata);
  addattr_nest_end(n, linfo);

  send_nlmsg(sock_fd, n);
}

static int get_netns_fd(pid_t pid)
{
  char path[PATH_MAX];
  snprintf(path, PATH_MAX, "/proc/%ld/ns/net", (long)pid);

  int fd = open(path, O_RDONLY);

  if (fd < 0)
  {
    DIE("Error opening NetNS %m");
  }

  return fd;
}

static void move_if_to_netns(int sock_fd, const char *ifname, int netns)
{
  struct nl_req req = {
      .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg)),
      .n.nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK,
      .n.nlmsg_type = RTM_NEWLINK,
      .i.ifi_family = PF_NETLINK,
  };

  addattr_l(&req.n, sizeof(req), IFLA_NET_NS_FD, &netns, sizeof(int));
  addattr_l(&req.n, sizeof(req), IFLA_IFNAME, ifname, strlen(ifname) + 1);

  send_nlmsg(sock_fd, &req.n);
}

static int parse_addrv4(__u8 *data, const char *addr)
{
  int i;

  for (i = 0; i < 4; ++i)
  {
    unsigned long n;
    char *endp;

    n = strtoul(addr, &endp, 0);

    if (n > 255)
    {
      return -1;
    }

    if (endp == addr)
    {
      return -1;
    }

    data[i] = n;
    if ('\0' == *endp)
    {
      break;
    }

    if (3 == i || '.' != *endp)
    {
      return -1;
    }

    addr = endp + 1;
  }

  return 1;
}

static void if_up(int sock_fd, const char *ifname, const char *addr)
{
  const int ifindex = if_nametoindex(ifname);
  if (ifindex == 0)
  {
    DIE("Invalid interface %s", ifname);
  }
  struct nla_req req = {
      .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg)),
      .n.nlmsg_flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST | NLM_F_ACK,
      .n.nlmsg_type = RTM_NEWADDR,

      .ifa.ifa_family = AF_INET,
      .ifa.ifa_prefixlen = 24,
      .ifa.ifa_index = ifindex,
      .ifa.ifa_scope = 0,
  };
  __u8 addr_data[4];
  parse_addrv4(addr_data, addr);
  addattr_l(&req.n, sizeof(req), IFA_LOCAL, addr_data, sizeof(addr_data));

  send_nlmsg(sock_fd, &req.n);

  struct nl_req nlreq = {
      .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg)),
      .n.nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK,
      .n.nlmsg_type = RTM_NEWLINK,

      .i.ifi_family = AF_UNSPEC,
      .i.ifi_type = 0, /* must be set to zero, otherwise this won't work */
      .i.ifi_index = ifindex,
      .i.ifi_flags = IFF_UP,
      .i.ifi_change = 0xFFFFFFFF,
  };

  addattr_l(&nlreq.n, sizeof(nlreq), IFLA_IFNAME, ifname, strlen(ifname) + 1);

  send_nlmsg(sock_fd, &nlreq.n);
}

static void add_default_route(int sock_fd, const char *ifname, const char *next_hop)
{
  const uint32_t ifindex = if_nametoindex(ifname);
  if (0 == ifindex)
  {
    DIE("Invalid interface %s", ifname);
  }

  struct nlr_req nlreq = {
      .n.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtmsg)),
      .n.nlmsg_flags = NLM_F_CREATE | NLM_F_EXCL | NLM_F_REQUEST | NLM_F_ACK,
      .n.nlmsg_type = RTM_NEWROUTE,

      .r.rtm_family = AF_INET,
      .r.rtm_dst_len = 0,
      .r.rtm_src_len = 0,
      .r.rtm_tos = 0,

      .r.rtm_table = RT_TABLE_MAIN,
      .r.rtm_protocol = RTPROT_BOOT,
      .r.rtm_type = RTN_UNICAST,
      .r.rtm_scope = RT_SCOPE_UNIVERSE,
  };

  __u8 addr_data[4];
  parse_addrv4(addr_data, next_hop);

  addattr_l(&nlreq.n, sizeof(nlreq), RTA_GATEWAY, addr_data, sizeof(addr_data));
  addattr_l(&nlreq.n, sizeof(nlreq), RTA_OIF, &ifindex, sizeof(uint32_t));

  send_nlmsg(sock_fd, &nlreq.n);
}

static const char *ip_forward_filename = "/proc/sys/net/ipv4/ip_forward";
static const uint8_t enabled = '1';

static inline void enable_ip_forward()
{
  /* enable ip forwarding */
  const int fd = open(ip_forward_filename, O_RDWR);
  if (-1 == fd)
  {
    DIE("Unable to open %s to enable ip forwarding: %m", ip_forward_filename);
  }
  if (1 != write(fd, &enabled, sizeof(uint8_t)))
  {
    close(fd);
    DIE("Unable to write file: %m");
  }

  close(fd);
}

static const size_t clear_size = sizeof(struct nlmsghdr) + sizeof(struct nfgenmsg);
static void start_batch(void *buf, void **nextbuf)
{

  struct nlf_req *req = (struct nlf_req *)buf;

  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST;
  req->n.nlmsg_type = NFNL_MSG_BATCH_BEGIN;
  req->n.nlmsg_seq = time(NULL);

  req->f.res_id = NFNL_SUBSYS_NFTABLES;

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void end_batch(void *buf, void **nextbuf)
{

  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST;
  req->n.nlmsg_type = NFNL_MSG_BATCH_END;
  req->n.nlmsg_seq = time(NULL);

  req->f.res_id = NFNL_SUBSYS_NFTABLES;

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void flush_rules(void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_DELTABLE;
  req->n.nlmsg_seq = time(NULL);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void add_nat_table(const char *table_name, void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_ACK;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_NEWTABLE;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;
  struct nlmsghdr *n = &req->n;
  addattr_l(n, sizeof(struct nlf_req), NFTA_TABLE_NAME, table_name, strlen(table_name) + 1);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void remove_nat_table(const char *table_name, void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_ACK;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_DELTABLE;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;
  struct nlmsghdr *n = &req->n;
  addattr_l(n, sizeof(struct nlf_req), NFTA_TABLE_NAME, table_name, strlen(table_name) + 1);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void add_nat_postrouting_chain(const char *chain_name, const char *table_name,
                          void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_NEWCHAIN;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;

  struct nlmsghdr *n = &req->n;
  const size_t maxlen = sizeof(struct nlf_req);

  addattr_l(n, maxlen, NFTA_CHAIN_TABLE, table_name, strlen(table_name) + 1);
  addattr_l(n, maxlen, NFTA_CHAIN_NAME, chain_name, strlen(chain_name) + 1);
  addattr_l(n, maxlen, NFTA_CHAIN_TYPE, "nat", 4);
  addattr_uint32(n, maxlen, NFTA_CHAIN_POLICY, htonl(NF_ACCEPT));

  struct rtattr *posthookdata = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_CHAIN_HOOK);
  addattr_uint32(n, maxlen, NFTA_HOOK_HOOKNUM, htonl(NF_INET_POST_ROUTING));
  addattr_uint32(n, maxlen, NFTA_HOOK_PRIORITY, htonl(100));
  addattr_nest_end(n, posthookdata);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void add_nat_prerouting_chain(const char *chain_name, const char *table_name,
                          void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_NEWCHAIN;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;

  struct nlmsghdr *n = &req->n;
  const size_t maxlen = sizeof(struct nlf_req);

  addattr_l(n, maxlen, NFTA_CHAIN_TABLE, table_name, strlen(table_name) + 1);
  addattr_l(n, maxlen, NFTA_CHAIN_NAME, chain_name, strlen(chain_name) + 1);
  addattr_l(n, maxlen, NFTA_CHAIN_TYPE, "nat", 4);
  addattr_uint32(n, maxlen, NFTA_CHAIN_POLICY, htonl(NF_ACCEPT));

  struct rtattr *prehookdata = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_CHAIN_HOOK);
  addattr_uint32(n, maxlen, NFTA_HOOK_HOOKNUM, htonl(NF_INET_PRE_ROUTING));
  addattr_uint32(n, maxlen, NFTA_HOOK_PRIORITY, htonl(0));
  addattr_nest_end(n, prehookdata);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static void add_forward_rule(const char *table_name, const char *chain_name,
  const char *target_addr, void *buf, void **nextbuf)
{
struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_ACK | NLM_F_APPEND;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_NEWRULE;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;

  struct nlmsghdr *n = &req->n;
  static const size_t maxlen = sizeof(struct nlf_req);

  addattr_string(n, maxlen, NFTA_RULE_TABLE, table_name);
  addattr_string(n, maxlen, NFTA_RULE_CHAIN, chain_name);

  struct rtattr *rule = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_RULE_EXPRESSIONS);
  struct rtattr *expr_forward = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_LIST_ELEM);

  addattr_nest_end(n, expr_forward);
  addattr_nest_end(n, rule);
}

static void add_masq_rule(const char *table_name, const char *chain_name,
                          const char *ifname, void *buf, void **nextbuf)
{
  struct nlf_req *req = (struct nlf_req *)buf;
  memset(req, 0, clear_size);

  req->n.nlmsg_len = NLMSG_LENGTH(sizeof(struct nfgenmsg));
  req->n.nlmsg_flags = NLM_F_REQUEST | NLM_F_CREATE | NLM_F_ACK | NLM_F_APPEND;
  req->n.nlmsg_type = NFNL_SUBSYS_NFTABLES << 8 | NFT_MSG_NEWRULE;
  req->n.nlmsg_seq = time(NULL);

  req->f.nfgen_family = AF_INET;
  req->f.version = 0;
  req->f.res_id = 0;

  struct nlmsghdr *n = &req->n;
  static const size_t maxlen = sizeof(struct nlf_req);

  addattr_string(n, maxlen, NFTA_RULE_TABLE, table_name);
  addattr_string(n, maxlen, NFTA_RULE_CHAIN, chain_name);

  struct rtattr *rule = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_RULE_EXPRESSIONS);
  struct rtattr *expr_masq = addattr_nest(n, maxlen, NLA_F_NESTED | NFTA_LIST_ELEM);
  addattr_string(n, maxlen, NFTA_EXPR_NAME, "masq");
  addattr_nest_end(n, expr_masq);

  addattr_nest_end(n, rule);

  if (nextbuf)
  {
    *nextbuf = (uint8_t *)buf + req->n.nlmsg_len;
  }
}

static const char *table_name = "kostak_nat";
static void remove_nat(int sock_fd)
{
  uint8_t msg[PAYLOAD_SIZE];
  void *next = msg;

  start_batch(next, &next);
  remove_nat_table(table_name, next, &next);
  end_batch(next, &next);

  const size_t payload_size = (uint8_t *)next - msg;

  struct iovec iov = {
      .iov_base = msg,
      .iov_len = payload_size,
  };

  struct msghdr msgheader = {
      .msg_name = NULL,
      .msg_namelen = 0,
      .msg_iov = &iov,
      .msg_iovlen = 1};

  ssize_t status = sendmsg(sock_fd, &msgheader, 0);
  if (status < 0)
    DIE("cannot talk to rtnetlink: %m\n");

  check_response(sock_fd);
}

static void add_nat(int sock_fd, const char *ifname)
{
  uint8_t msg[PAYLOAD_SIZE * 4];
  void *next = msg;

  start_batch(next, &next);
  flush_rules(next, &next);
  add_nat_table(table_name, next, &next);
  add_nat_prerouting_chain("prerouting", table_name, next, &next);
  add_nat_postrouting_chain("postrouting", table_name, next, &next);
  add_masq_rule(table_name, "postrouting", "eth0", next, &next);
  end_batch(next, &next);

  const size_t payload_size = (uint8_t *)next - msg;

  struct iovec iov = {
      .iov_base = msg,
      .iov_len = payload_size,
  };

  struct msghdr msgheader = {
      .msg_name = NULL,
      .msg_namelen = 0,
      .msg_iov = &iov,
      .msg_iovlen = 1};

  ssize_t status = sendmsg(sock_fd, &msgheader, 0);
  if (status < 0)
    DIE("cannot talk to rtnetlink: %m\n");

  check_response(sock_fd);
}

void prepare_netns(pid_t cmd_pid,
                   const char *veth_name,
                   const char *vpeer_name,
                   const char *veth_addr,
                   const char *vpeer_addr,
                   const char *veth_netmask)
{

  const int sock_fd = create_socket(
      PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);

  create_veth(sock_fd, veth_name, vpeer_name);

  const int current_netns = get_netns_fd(getpid());
  const int child_netns = get_netns_fd(cmd_pid);

  move_if_to_netns(sock_fd, vpeer_name, child_netns);

  if_up(sock_fd, veth_name, veth_addr);

  if (setns(child_netns, CLONE_NEWNET))
  {
    DIE("Error entering child ns for pid %ld : %m", (long)cmd_pid);
  }

  const int sock_fd_ns = create_socket(
      PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);

  if_up(sock_fd_ns, vpeer_name, vpeer_addr);
  add_default_route(sock_fd_ns, vpeer_name, veth_addr);
  close(sock_fd_ns);

  if (setns(current_netns, CLONE_NEWNET))
  {
    DIE("Error coming back to parent ns: %m");
  }
  close(sock_fd);

  enable_ip_forward();
  
  const int sock_fd_filter = create_socket(PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_NETFILTER);

  add_nat(sock_fd_filter, veth_name);
  close(sock_fd_filter);
  
}

void cleanup_netns()
{
  const int sock_fd_filter = create_socket(PF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_NETFILTER);
  remove_nat(sock_fd_filter);
  close(sock_fd_filter);
} 
