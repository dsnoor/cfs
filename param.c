/*
 * Kostak A Simple Container Example
 *
 * Copyright (C) 2020 Didiet Noor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "param.h"

static inline void print_usage()
{
  fprintf(stderr,
          "Usage: kostak [-r rootfs] [-n] [-h] [-e env_var] <program>\n"
          "All options are optional and have default values\n"
          " -n\t\tenable and disable network namespaces [default: disabled]\n"
          " -r <rootfs>\tspecify rootfs for chroot [default: '$PWD/rootfs']\n"
          " -h\t\tshow this help\n"
          " -e <env_var>\tSpecify environment variable to be passed to child\n"
          " \t\tEnvironment variable should be in 'FOO=bar' format\n"
  );
}

void parse_args(int argc, char **argv, struct exec_param *param) {
  if (argc < 2) {
    print_usage();
    exit(EXIT_FAILURE);
  }

  memset(param, 0, sizeof(struct exec_param));

  int c = 0;
  int option_index = 0;
  int env_count = 2; // first 2 is pre-filled

  const static struct option long_options[] = {
    {"network", no_argument, 0, 'n'},
    {"help", no_argument, 0, 'h'},
    {"root", required_argument, 0, 'r'},
    {"env", required_argument, 0, 'e'},
    {0,0,0,0},
  };

  for(;;) {
    if (c == -1) {
      break;
    }
    c = getopt_long(argc, argv, "nhr:e:", long_options, &option_index);

    switch(c) {
      case 'n':
        param->is_netns = true;
        break;
      case 'h':
        print_usage();
        exit(EXIT_SUCCESS);
        break;
      case 'r':
        param->root = optarg;
        break;
      case 'e':
        if(env_count < MAX_ENV_COUNT) {
          if (strchr(optarg, '=')) {
            param->envp[env_count++] = optarg;
          } else {
            LOG("Warning: not a correct environment variables: '%s', IGNORED\n", optarg);
          }
        } else {
          LOG("Warning: environment exceeded count, discarding\n");
        }
        break;
      case '?':
        print_usage();
        exit(EXIT_FAILURE);
      default:
        break;
    }

    param->argv = &argv[optind];
    param->program_name = param->argv[0];
    param->env_count = env_count - 2;
  }
}