#!/bin/sh

set -ex

dnf -y install epel-release
dnf -y install git mercurial python3 gcc-c++ gcc make cmake gdb kernel-devel kernel-headers wget curl debootstrap

BASE_ROOTFS=/var/lib/kostak/rootfs
ALPINE_ROOTFS=${BASE_ROOTFS}/alpine
RHEL_ROOTFS=${BASE_ROOTFS}/rhel
DEBIAN_ROOTFS=${BASE_ROOTFS}/debian

mkdir -p ${BASE_ROOTFS} ${ALPINE_ROOTFS} ${RHEL_ROOTFS} ${DEBIAN_ROOTFS}

cd ${ALPINE_ROOTFS}
curl -sSL http://dl-cdn.alpinelinux.org/alpine/v3.11/releases/x86_64/alpine-minirootfs-3.11.5-x86_64.tar.gz | gzip -dc - | tar xvf -

cd /

dnf -y --installroot=${RHEL_ROOTFS} --releasever=8 groups install 'Minimal Install'
dnf -y --installroot=${RHEL_ROOTFS} --releasever=8 install procps-ng python3 which iproute net-tools

debootstrap --arch=amd64 buster ${DEBIAN_ROOTFS} http://kebo.pens.ac.id/debian/


