/*
 * Kostak A Simple Container Example
 *
 * Copyright (C) 2020 Didiet Noor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARAM_H
#define PARAM_H 1

#define MAX_ENV_COUNT 1024

struct exec_param
{
  char **argv;
  char *program_name;
  bool is_netns;
  char *root;
  char *envp[1024];
  int  env_count;
};

void parse_args(int argc, char **argv, struct exec_param *param);

#endif /* PARAM_H */