/*
 * Kostak A Simple Container Example
 *
 * Copyright (C) 2020 Didiet Noor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "mount_ns.h"
#include "net_ns.h"
#include "param.h"

static const char *child_hostname = "kostak";
static const char *default_rootfs = "rootfs";

static int child_exec(void *stuff)
{
  struct exec_param *args = (struct exec_param *)stuff;
  static char *default_envp[] = {
      "PS1=\\u@\\h:\\w\\$ ",
      "TERM=screen",
      "PATH=/usr/local/bin:/usr/bin:/bin:/sbin:/usr/sbin:/usr/local/sbin",
      NULL};

  if (-1 == sethostname(child_hostname, strlen(child_hostname)))
  {
    DIE("Failure when setting hostname: %m");
  }

  change_root(args->root != NULL ? args->root : default_rootfs);

  if (args->env_count == 0) {
    memcpy(args->envp, default_envp, sizeof(default_envp));
  } else {
    memcpy(args->envp, default_envp, 2 * sizeof (char*));
  }

  if (-1 == execve(args->program_name, args->argv, args->envp))
  {
    DIE("Cannot execute %s: %m", args->program_name);
  }

  /* The code shouldn't arrive here */
  return EXIT_FAILURE;
}

int main(int argc, char **argv)
{
  struct exec_param params;
  parse_args(argc, argv, &params);

  char *stack, *stack_top;

  stack = mmap(NULL, CHILD_STACK_SIZE, PROT_READ | PROT_WRITE,
               MAP_STACK | MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

  if (MAP_FAILED == stack)
  {
    DIE("Error allocating stack: %m");
  }
  stack_top = stack + CHILD_STACK_SIZE;

  int clone_flags = SIGCHLD 
    | CLONE_NEWNS 
    | CLONE_NEWPID 
    | CLONE_NEWUTS;

  if (params.is_netns) {
    clone_flags |= CLONE_NEWNET;
  }

  const pid_t child_pid = clone(child_exec, stack_top, clone_flags, &params);

  if (-1 == child_pid)
  {
    DIE("Failure cloning new process: %m");
  }

  LOG("> Parent %ld - child %ld\n", (long)getpid(), (long)child_pid);

  if (params.is_netns) {
  prepare_netns(
      child_pid,
      "veth0", "veth1", "10.0.0.1", "10.0.0.2", "255.255.255.0");
  }

  if (-1 == waitpid(child_pid, NULL, 0))
  {
    DIE("Cannot wait for pid %ld: %m", (long)child_pid);
  }

  cleanup_netns();
  LOG("> Child %ld exited\n", (long)child_pid);

  return EXIT_SUCCESS;
}
